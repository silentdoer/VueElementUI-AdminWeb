import Vue from 'vue';
import App from './App.vue';
// 默认导入包内的index.js，可以写完整路径即from './router/index.js'，在这里面有:
/*
import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);
而router是Router的一个实例
* */
import router from './router';
import ElementUI from 'element-ui';
import VueI18n from 'vue-i18n';
/*
package.json是同时满足npm和yarn的，它们能公用同一个package.json配置，
然后package.json里的dependencies和devDependencies来自下面的两种命令
其中dependencies是生产环境需要用到的依赖，而devDependencies则只是开发的时候需要用到，打包后不会存在（类似lombok）
比如vue-cli就是devDependencies，它只是开发时的一个工具，打包编译后不会再依赖它所以放devDependencies里
npm install module-name -save 自动把模块和版本号添加到dependencies部分
npm install module-name -save-dev 自动把模块和版本号添加到devDependencies部分
* */
// 导入i18n.js里配置的messages对象
import { messages } from './components/common/i18n';
import 'element-ui/lib/theme-chalk/index.css'; // 默认主题
//import './assets/css/theme-green/index.css'; // 浅绿色主题
// 全局的css（即包括在public/index.html的head里引用的）都可以在这里引入自动就会置为全局css，在App.vue里@import也行
import './assets/css/icon.css';
import './assets/css/font-icon-ali.css';
import './components/common/directives';
import 'babel-polyfill';
// TODO 项目根目录下的public/index.html不能删，虽然不知道vue在什么地方用到和为什么不放到src下，但是确实是需要的东西
Vue.config.productionTip = false;
Vue.use(VueI18n);
// use是使用插件而非是使用component
// component是用Vue.component('component-name', {template:'...'});来引入的
// component可以只对单个Vue对象生效，即vue对象components属性里添加
Vue.use(ElementUI, {
    size: 'small'
});
const i18n = new VueI18n({
    locale: 'zh',
    /*messages对应着i18n.js里的messages对象，上面的locale则是设置用什么语言的key*/
    messages
});

/**
 * 当 npm run serve 时，process.env.NODE_ENV的值为development
 * 当 npm run test 时，process.env.NODE_ENV的值为test （需在scripts手动添加test）
 * 当 npm run build 时，process.env.NODE_ENV的值为production
 *
 * 而且注意serve是支持动态部署的，即在npm run serve后，如果修改了组件，那么这个修改是会实时更新
 * 而不需要重新再npm run serve一次的；
 *
 * 对于xxx.json文件里属性必须是以""括起来而不能是''
 *
 * 根据标准，html才是页面的根元素，然后head和body是唯二的子元素；所以script style之类的要么写在head
 * ，要么写在body里，而最好不写在body外面【当然浏览器也能解析】
 */

//使用钩子函数对路由进行权限跳转
router.beforeEach((to, from, next) => {
    if (to.meta !== undefined && to.meta !== null && to.meta.title !== undefined && to.meta.title !== null) {
        document.title = `${to.meta.title} | Backend-ManageSystem`;
    } else if (to.path === '/login') {
        document.title = '登录 | Backend-ManageSystem';
    }
    // localStorage浏览器本地缓存，这里用于缓存类似token
    const role = localStorage.getItem('ms_username');
    if (!role && to.path !== '/login') {
        next('/login');
    } else if (to.meta.permission) {
        // 描述if：这里的to是只要访问的页面，然后meta是该页面在Router里的元信息
        // ，而permission就是指/permission的router数据里多了meta: { title: '权限测试', permission: true }这个permission: true
        // ，需要权限验证的页面都可以加上permission: true然后下面就是判断此用户是否有权限

        // TODO 如果用非admin用户访问/permission页面会跳转到/403页面里去
        // 其他页面可以理解为访客页面
        // 如果是管理员权限则可进入，这里只是简单的模拟管理员权限而已
        role === 'admin' ? next() : next('/403');
    } else {
        // 简单的判断IE10及以下不进入富文本编辑器，该组件不兼容
        if (navigator.userAgent.indexOf('MSIE') > -1 && to.path === '/editor') {
            Vue.prototype.$alert('vue-quill-editor组件不兼容IE10及以下浏览器，请使用更高版本的浏览器查看', '浏览器不兼容通知', {
                confirmButtonText: '确定'
            });
        } else {
            next();
        }
    }
});

// 没有el: '#app'，所以后面用$mount手动挂载
new Vue({
    router,
    i18n,
    render: h => h(App)
}).$mount('#app');
