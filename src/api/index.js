import request from '../utils/request';

// 获取表格数据BaseTable.vue里的
export const fetchData = (query) => {
    return request({
        url: '/ms/table/list',
        method: 'post',
        data: query
    })
};