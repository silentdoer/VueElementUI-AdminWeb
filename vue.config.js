// 这是es6的写法，但是貌似vue.config.js不支持（或者说是node不支持）
// 支持，但是要以.mjs结尾，但是vue内部的js则不需要，因为它会被vue-cli进行解析
// 即如果要用es2015的module，需要将项目根目录里的.js改成.mjs，src里的不需要；
// 但是经过测试，export的方式即便是在.mjs里不会报错却不会生效，所以还是乖乖在项目配置里用Common Js标准，具体的代码倒是可以es6
// 不过这个问题应该是vue-cli3.x里没有支持vue.config.mjs导致的，所以没有找到vue.config.js就用了默认的配置（端口8080）
//import todoListMockData from './mock/todoList.json';
// 这种是common js标准里的写法
const todoListMockData = require('./mock/todoList.json');
// CommonJs里的module.exports是值的导出（如导出a.js），b.js进行require(..)是会拷贝一个a.js导出的对象过去
// ，然后在b.js里修改该对象不会影响到a.js里的对象的值，但是export则是引用导出，b.js修改了导出值a.js里也会收到影响
// ，这个也是Vue EventBus用一个共用的Vue对象的实现原理
// 注意，module.exports是Common JS的规范，如果要用ES2015的要将.js改成.mjs（module js），这样能被node识别
module.exports = {
	// 貌似vue.config.js是vue-cli 3.x版本提供的配置文件，可以不再需要webpack来打包（当然也可能是vue-cli 3.x内部用了webpack）
	publicPath: './',
	assetsDir: 'assets',
	// 输出目录
	outputDir: 'dist',
	productionSourceMap: false,
	// webpack配置
	chainWebpack: () => {
	},
	configureWebpack: {
		// 警告 webpack 的性能提示
		performance: {
			hints: 'warning',
			// 入口起点的最大体积
			maxEntrypointSize: 50000000,
			// 生成文件的最大体积
			maxAssetSize: 30000000,
			// 只给出 js 文件的性能提示
			assetFilter: function (assetFilename) {
				return assetFilename.endsWith('.js');
			}
		}
	},
	devServer: {
		host: 'localhost',
		port: 8866,
		/*proxy的作用是通过npm run serve时，应用里如果有http请求，比如fetch('/user-service/test',...)
		* ，那么这个请求 不会 转发到http://localhost:8866/user-service/test
		* ，而是变成http://localhost:8080/test，注意是/test没有前面的/user-service
		* ，如果要变成http://localhost:8080/user-service/test
		* pathRewrite要改成'/user-service': '/user-service'才行
		* ，有了这个可以方便调试前端项目的同时请求后端接口，它的原理其实就是nginx里的反向代理
		* ，这里第一个/user-service是表示声明一个转发的uri前缀，当匹配到是以/user-service开头
		* 的请求时就会通过这个proxy item来转发到8080端口的服务端里，这里应该可以定义多个proxy item
		* ，第二个/user-service只是定义这个请求是否需要进行地址的映射；*/
		/*proxy: {
			'/user-service': {
				target: 'http://localhost:8080',
				changeOrigin: true,
				pathRewrite: {
					'/user-service': ''
				}
			}
		},*/
		/*before是vue-cli3.x提供的用于模拟服务端接口，这里可以和Dashboard.vue里的fetchTodoList方法一起看*/
		before(app) {
			app.get('/app-context/dashboard/todoList', (req, resp, next) => {
				resp.json(todoListMockData);
			});

			app.get('/app-context/dashboard/otherList', (req, resp) => {
				resp.json({name: 'silentdoer'});
			});

			// 注意要区分模拟的接口是POST还是其他method类型
			app.post('/app-context/dashboard/postTest', (req, resp) => {
				resp.json({});
			});
		}
	},
	// 第三方插件配置
	pluginOptions: {
		// 添加图标，注意vue-cli3.x项目里添加图标不是在public/index.html里添加一个link，而是通过这里配置
		// ，图标位置也是放置public目录里（且编译后也和index.html在同一目录），这里的相对目录就是相对public目录
		// 【重要】这种配置是能够令项目serve时显示图标，但是如果将编译好的项目单独打开则图标失效，
		// 要想这种情况图标也有效还是要在public/index.html里添加<link rel="shortcut icon" type="image/x-icon" href="./favicon.ico"/>
		// 放到<title>下面即可
		pwa: {
			iconPaths: {
				favicon32: './favicon.ico',
				favicon16: './favicon.ico',
				appleTouchIcon: './favicon.ico',
				maskIcon: './favicon.ico',
				msTileImage: './favicon.ico'
			}
		}
	}
};